import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../services/chat.service';
import { AppService } from '../../services/app.service';
import { Router } from '@angular/router';
import { FormControl, FormGroupDirective, NgForm, Validators } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})

export class TermsComponent implements OnInit {
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email
  ]);

  idFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(7)
  ]);

  userEmail: string;
  userId: string;

  constructor(private chatService: ChatService, private router: Router, private appService: AppService ) { 
    this.userEmail = "";
    this.userId = "";
    this.chatService = chatService;
    this.router = router;
    this.appService = appService;
  }

  ngOnInit( ) {
    //generate random 7 digit number here

    for(var i = 0; i < 7; i++) { 
      var numToAdd = '' + Math.floor(Math.random() * Math.floor(10));
      this.userId = this.userId + numToAdd;
    }

    console.log(this.userId);
  }

  submit() {
    //this.chatService.userEmail = this.userEmail;
    this.chatService.userId = this.userId;
    
    this.navigate();
  }

  navigate() {
    this.router.navigate(['/chat']);
  }
}
