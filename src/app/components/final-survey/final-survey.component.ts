import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { ChatService } from '../../services/chat.service';
import { AppService } from '../../services/app.service';
import { SurveyData, DemographicsSurveyData } from '../../models/studyData';
import { Questions, Questions2 } from '../chat/survey-dialog/survey-dialog.component';

enum APPENDICES {
  B = "Appendix B",
  F = "Appendix F",
  G = "Appendix G",
  H = "Appendix H",
  I = "Appendix I",
  J = "Appendix J"
}

@Component({
  selector: 'app-final-survey',
  templateUrl: './final-survey.component.html',
  styleUrls: ['./final-survey.component.css']
})

export class FinalSurveyComponent implements OnInit {
  title: string;
  displayedColumns = [
    "question",
    "StronglyDisagree",
    "SomewhatDisagree",
    "Neither",
    "SomewhatAgree",
    "StronglyAgree"
  ];

  displayedColumns2 = [
    "question",
    "StronglyDisagree",
    "SomewhatDisagree",
    "Neither",
    "SomewhatAgree",
    "StronglyAgree",
    "Six",
    "Seven"
  ];

  dataSource = new MatTableDataSource<Questions>(APENDIX_QUESTIONS_F);
  dataSource2 = new MatTableDataSource<Questions2>(APENDIX_QUESTIONS_G);
  dataSource3 = new MatTableDataSource<Questions2>(APENDIX_QUESTIONS_H);
  dataSource4 = new MatTableDataSource<Questions>(APENDIX_QUESTIONS_I);
  dataSource5 = new MatTableDataSource<Questions>(APENDIX_QUESTIONS_J);
  dataSource6 = new MatTableDataSource<Questions>(APENDIX_QUESTIONS_B1);
  dataSource7 = new MatTableDataSource<Questions2>(APENDIX_QUESTIONS_B2);

  surveyFinalSubmitted: boolean;
  // surveyDemoSubmitted: boolean;
  showDebrief: boolean;
  userId: string;

  constructor(private chatService: ChatService, private appService: AppService) { 
    this.surveyFinalSubmitted = false;
    // this.surveyDemoSubmitted = false;

    this.chatService = chatService;
    this.appService = appService;
    this.showDebrief = false;
    this.userId = "";
  }

  ngOnInit() {
    this.userId = this.chatService.userId;
  }

//  showFinalSurvey() {
//     this.showDebrief = false;
//  }

  showDemoSurvey() {
    this.surveyFinalSubmitted = true;
    this.showDebrief = false;
  }

  //MARK: - TODO
  //ADD Demo Survey Data Save
  submitSurvey() {
    //save data in chat service
    var questionOneData: string[] = [];
    var questionTwoData: string[] = [];
    var questionThreeData: string[] = [];
    var questionFourData: string[] = [];
    var questionFiveData: string[] = [];
    var questionSixData: string[] = [];
    var questionSevenData: string[] = [];

    //var ethnicBackgroundStrings: string[] = [];

    for (var i = 0; i < APENDIX_QUESTIONS_F.length; i++ ) {
     questionOneData.push(APENDIX_QUESTIONS_F[i].selection == '' ? "No response" : APENDIX_QUESTIONS_F[i].selection);
    // questionResponse = APENDIX_QUESTIONS_C[i].selection == '' ? "No response" : APENDIX_QUESTIONS_C[i].selection
    } 
    for (var i = 0; i < APENDIX_QUESTIONS_G.length; i++ ) {
      questionTwoData.push(APENDIX_QUESTIONS_G[i].selection == '' ? "No response" : APENDIX_QUESTIONS_G[i].selection);
      //questionTwoData.push(APENDIX_QUESTIONS_G[i].selection);
    } 
    for (var i = 0; i < APENDIX_QUESTIONS_H.length; i++ ) {
      questionThreeData.push(APENDIX_QUESTIONS_H[i].selection == '' ? "No response" : APENDIX_QUESTIONS_H[i].selection);
     // questionThreeData.push(APENDIX_QUESTIONS_H[i].selection);
    } 
    for (var i = 0; i < APENDIX_QUESTIONS_I.length; i++ ) {
      questionFourData.push(APENDIX_QUESTIONS_I[i].selection == '' ? "No response" : APENDIX_QUESTIONS_I[i].selection);
   //   questionFourData.push(APENDIX_QUESTIONS_I[i].selection);
    } 
    for (var i = 0; i < APENDIX_QUESTIONS_J.length; i++ ) {
      questionFiveData.push(APENDIX_QUESTIONS_J[i].selection == '' ? "No response" : APENDIX_QUESTIONS_J[i].selection);
   //   questionFourData.push(APENDIX_QUESTIONS_J[i].selection);
    } 
    for (var i = 0; i < APENDIX_QUESTIONS_B1.length; i++ ) {
      questionSixData.push(APENDIX_QUESTIONS_B1[i].selection == '' ? "No response" : APENDIX_QUESTIONS_B1[i].selection);
   //   questionFourData.push(APENDIX_QUESTIONS_J[i].selection);
    } 
    for (var i = 0; i < APENDIX_QUESTIONS_B2.length; i++ ) {
      questionSevenData.push(APENDIX_QUESTIONS_B2[i].selection == '' ? "No response" : APENDIX_QUESTIONS_B2[i].selection);
   //   questionFourData.push(APENDIX_QUESTIONS_J[i].selection);
    } 

    var finalSurveyData = new SurveyData(questionOneData, questionTwoData, questionThreeData, questionFourData, questionFiveData, questionSixData, questionSevenData);
    //var demoSurveyData = new DemographicsSurveyData(this.userGender, ethnicBackgroundStrings, this.userAge, this.userYearInCollege, this.userHasWorkExperience);

    this.chatService.finalSurveyData = finalSurveyData;
   // this.chatService.demographicsSurveyData = demoSurveyData;
    
    this.chatService.endSurveyTimer( () => {
      this.postSurveyData( () => {
        this.surveyFinalSubmitted = true;
        this.showDebrief = true;
      });
      //get rid of this after app is working properly at end
      this.surveyFinalSubmitted = true;
      this.showDebrief = true;
    });
  }

  postSurveyData( callback: () => void) {
    var responsesQuestionOne = [];
    var responsesQuestionTwo = [];
    var responsesQuestionThree = [];
    var responsesQuestionFour = [];
    var responsesQuestionFive = [];
    var responsesQuestionSix = [];
    var responsesQuestionSeven = [];

    for (var i = 0; i < APENDIX_QUESTIONS_F.length; i++ ) {
      responsesQuestionOne.push({roundNumber: APENDIX_QUESTIONS_F[i].question, messageText: APENDIX_QUESTIONS_F[i].selection});
    } 
    for (i = 0; i < APENDIX_QUESTIONS_G.length; i++ ) {
      responsesQuestionTwo.push({roundNumber: APENDIX_QUESTIONS_G[i].question, messageText: APENDIX_QUESTIONS_G[i].selection});
    }
    for (i = 0; i < APENDIX_QUESTIONS_H.length; i++ ) {
      responsesQuestionThree.push({roundNumber: APENDIX_QUESTIONS_H[i].question, messageText: APENDIX_QUESTIONS_H[i].selection});
    }
    for (i = 0; i < APENDIX_QUESTIONS_I.length; i++ ) {
      responsesQuestionFour.push({roundNumber: APENDIX_QUESTIONS_I[i].question, messageText: APENDIX_QUESTIONS_I[i].selection});
    }
    for (i = 0; i < APENDIX_QUESTIONS_J.length; i++ ) {
      responsesQuestionFive.push({roundNumber: APENDIX_QUESTIONS_J[i].question, messageText: APENDIX_QUESTIONS_J[i].selection});
    }
    for (i = 0; i < APENDIX_QUESTIONS_B1.length; i++ ) {
      responsesQuestionSix.push({roundNumber: APENDIX_QUESTIONS_B1[i].question, messageText: APENDIX_QUESTIONS_B1[i].selection});
    }
    for (i = 0; i < APENDIX_QUESTIONS_B2.length; i++ ) {
      responsesQuestionSeven.push({roundNumber: APENDIX_QUESTIONS_B2[i].question, messageText: APENDIX_QUESTIONS_B2[i].selection});
    }

    var resultsQuestionOne = {questionText: APPENDICES.F, ratingScale: "7", userResponses: responsesQuestionOne};
    var resultsQuestionTwo = {questionText: APPENDICES.G, ratingScale: "7", userResponses: responsesQuestionTwo};
    var resultsQuestionThree = {questionText: APPENDICES.H, ratingScale: "7", userResponses: responsesQuestionThree};
    var resultsQuestionFour = {questionText: APPENDICES.I, ratingScale: "5", userResponses: responsesQuestionFour};
    var resultsQuestionFive = {questionText: APPENDICES.J, ratingScale: "5", userResponses: responsesQuestionFive};
    var resultsQuestionSix = {questionText: APPENDICES.B, ratingScale: "5", userResponses: responsesQuestionSix};
    var resultsQuestionSeven = {questionText: APPENDICES.B, ratingScale: "7", userResponses: responsesQuestionSeven};

    var surveyQuestions = [resultsQuestionOne, resultsQuestionTwo, resultsQuestionThree, resultsQuestionFour, resultsQuestionFive, resultsQuestionSix, resultsQuestionSeven];

    var surveyData = {round: "final survey", surveyQuestions};

    console.log(surveyData);

    this.appService.postSurveyData(this.chatService.userId, surveyData).subscribe( (x) => {
      this.appService.postTimeElapsed(this.chatService.userId, this.chatService.timeElapsedString).subscribe((x) => {
        callback();
      });
    });
  }

  setQuestionOne( element, elementPos ) {
    APENDIX_QUESTIONS_F[elementPos - 1].selection = element;
  }

  setQuestionTwo( element, elementPos ) {
    APENDIX_QUESTIONS_G[elementPos - 1].selection = element;
  }

  setQuestionThree( element, elementPos ) {
    APENDIX_QUESTIONS_H[elementPos - 1].selection = element;
  }

  setQuestionFour( element, elementPos ) {
    APENDIX_QUESTIONS_I[elementPos - 1].selection = element;
  }

  setQuestionFive( element, elementPos ) {
    APENDIX_QUESTIONS_J[elementPos - 1].selection = element;
  }

  setQuestionSix( element, elementPos ) {
    APENDIX_QUESTIONS_B1[elementPos - 1].selection = element;
  }
  setQuestionSeven( element, elementPos ) {
    APENDIX_QUESTIONS_B2[elementPos - 1].selection = element;
  }

  surveyCompleted(): boolean {
    for (var i = 0; i < APENDIX_QUESTIONS_F.length; i++ ) {
      if(APENDIX_QUESTIONS_F[i].selection == '') {
        return false;
      }
    } 
    for (i = 0; i < APENDIX_QUESTIONS_G.length; i++ ) {
      if(APENDIX_QUESTIONS_G[i].selection == '') {
        return false;
      }
    }
    for (i = 0; i < APENDIX_QUESTIONS_H.length; i++ ) {
      if(APENDIX_QUESTIONS_H[i].selection == '') {
        return false;
      }
    }
    for (i = 0; i < APENDIX_QUESTIONS_I.length; i++ ) {
      if(APENDIX_QUESTIONS_I[i].selection == '') {
        return false;
      }
    }
    for (i = 0; i < APENDIX_QUESTIONS_J.length; i++ ) {
      if(APENDIX_QUESTIONS_J[i].selection == '') {
        return false;
      }
    }
    for (i = 0; i < APENDIX_QUESTIONS_B1.length; i++ ) {
      if(APENDIX_QUESTIONS_B1[i].selection == '') {
        return false;
      }
    }
    for (i = 0; i < APENDIX_QUESTIONS_B2.length; i++ ) {
      if(APENDIX_QUESTIONS_B2[i].selection == '') {
        return false;
      }
    }
    return true;
  }

  
}

const APENDIX_QUESTIONS_F: Questions2[] = [
  {
    position: 1,
    question: "I controlled my emotions by changing the way I thought about the negotiation",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 2,
    question: "When I wanted to feel less negative emotion, I changed the way I was thinking about the negotiation.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 3,
    question: "When I wanted to feel more positive emotion, I changed the way I was thinking about the negotiation.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 4,
    question: "I changed what I was thinking about when I wanted to feel more positive emotion (such as joy or amusement)",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 5,
    question: "I changed what I was thinking about when I wanted to feel less negative emotion (such as sadness or anger)",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  }
];

//f, g, h, i, j

const APENDIX_QUESTIONS_G: Questions2[] = [
  {
    position: 1,
    question: "During the negotiation, I controlled my emotions by not expressing them.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 2,
    question: "When I felt negative emotions, I made sure not to express them to the other party.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 3,
    question: "I kept my emotions to myself during the negotiation.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 4,
    question: "When I felt positive emotions, I made sure to not express them to the other party.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  }
];

const APENDIX_QUESTIONS_H: Questions2[] = [
  {
    position: 1,
    question: "During the negotiation, I hardly thought about my goals.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 2,
    question: "During the negotiation, I paid a lot of attention to the information about the negotiator's intentions.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 3,
    question: "During the negotiation, I made my offers without thinking too much.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 4,
    question: "During the negotiation, I tried to consider all the available information before placing an offer.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  }
];

const APENDIX_QUESTIONS_I: Questions[] = [
  {
    position: 1,
    question: "I felt uncomfortable because the other party's meaning, or intentions, were unclear to me.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 2,
    question: "I dislike unpredictable situations that are uncertain such as this negotiation.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 3,
    question: "I don't like situations that are uncertain such as this negotiation.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 4,
    question: "I don't like to go into a situation without knowing what I can expect from it, like this negotiation.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 5,
    question: "I liked to know what the other party was thinking of me throughout the negotiation.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 6,
    question: "When I was confused about an important issue during the negotiation, I felt very upset.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 7,
    question: "I enjoyed the uncertainty of going into a new situation without knowing what might happen, such as this negotiation.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  }
];
const APENDIX_QUESTIONS_J: Questions[] = [
  {
    position: 1,
    question: "The emotions I expressed to the other party were genuine.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 2,
    question: "The emotions I expressed to the other party matched what I spontaneously felt.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 3,
    question: "The emotions I expressed to the other party came naturally.",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  }
];

const APENDIX_QUESTIONS_B1: Questions[] = [
  {
    position: 1,
    question: "During the negotiation I felt that I depended on the buyer",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 2,
    question: "I felt that I had a strong negotiation position",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 3,
    question: "During the negotiation I felt that I needed the buyer to make a good deal",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 4,
    question: "I felt powerful in the negotiation",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 5,
    question: "I felt that I needed the buyer badly to earn an acceptable amount of points",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 6,
    question: "During the negotiation I did not feel dependent on the buyer",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 7,
    question: "The fact that I had an alternative offer made me feel relaxed",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 8,
    question: "The fact that I had an alternative offer gave me a sense of power in the negotiation",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 9,
    question: "During the negotiation, I felt that I was in control of the situation",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  }
];

const APENDIX_QUESTIONS_B2: Questions2[] = [
  {
    position: 1,
    question: "Who do you think had the stronest position in the negotiation?",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 2,
    question: "Who do you feel had the most influence on the course of the negotiation?",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 3,
    question: "Who do you feel had the most power in the negotiation?",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 4,
    question: "Who do you think had the best basis to negotiate?",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 5,
    question: "Who do you think had the best negotiation position?",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 6,
    question: "Who do you feel was most in control of the situation?",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 7,
    question: "Who do you feel was the most powerful person in the negotiation?",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 8,
    question: "Who do you think was most dependent on the other during the negotiation?",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 9,
    question: "Who do you feel needed the other most during the negotiation?",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  }
];
const DEMOGRAPHICS_QUESTIONS: any[] = [
  {
    position: 1, 
    question: "Please select the gender identity you identify with most. (Select one)",
    male: "1",
    female: "2",
    nonBinary: "3",
    other: "4",
    otherText: "",
    selection: ""
  },
  {
    position: 2,
    question: "Please select all options that describe your racial and ethnic identities. (Select multiple)",
    white: "1",
    latinx: "2",
    black: "3",
    islander: "4",
    eastAsian: "5",
    southAsian: "6",
    nativeAmerican: "7",
    selection: [String]
  },
  {
    position: 3,
    question: "Please indicate your age.",
    age: "",
  },
  {
    position: 4,
    question: "Please indicate your current year in college.",
    freshman: "1",
    sophomore: "2",
    junior: "3",
    senior: "4",
    selection: "" 
  },
  {
    position: 5,
    question: "Do you have any paid work experience?",
    yes: 1,
    no: 2,
    selection: ""
  }
];