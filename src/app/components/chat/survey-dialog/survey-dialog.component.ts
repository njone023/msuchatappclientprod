import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatTableDataSource, MatDialogRef } from '@angular/material';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { ChatService } from '../../../services/chat.service';
import { SurveyData } from '../../../models/studyData';
import { AppService } from '../../../services/app.service'; 

enum APPENDICES {
  C = "Appendix C",
  D = "Appendix D",
  E = "Appendix E",
  K = "Appendix K"
}

@Component({
  selector: 'app-survey-dialog',
  templateUrl: './survey-dialog.component.html',
  styleUrls: ['./survey-dialog.component.css'],
  animations: [trigger('flyInOut', [
    state('enterLeft', style({ transform: 'translateX(0)' })),
    state('enterRight', style({ transform: 'translateX(0)' })),
    transition('void => enterLeft', [
        style({ transform: 'translateX(100%)' }),
        animate('500ms ease-out')
    ]),
    transition('void => enterLeft', [
        style({ transform: 'translateX(100%)' }),
        animate('500ms ease-out')
    ]),
    transition('void => enterRight', [
        style({ transform: 'translateX(-100%)' }),
        animate('500ms ease-out')
    ])
  ])]
})

export class SurveyDialogComponent implements OnInit {
  title: string;
  navigationState: String;

  displayedColumns = [
    "question",
    "StronglyDisagree",
    "SomewhatDisagree",
    "Neither",
    "SomewhatAgree",
    "StronglyAgree"
  ];

  displayedColumns2 = [
    "question",
    "StronglyDisagree",
    "SomewhatDisagree",
    "Neither",
    "SomewhatAgree",
    "StronglyAgree",
    "Six",
    "Seven"
  ];

  dataSource = new MatTableDataSource<Questions>(APENDIX_QUESTIONS_C);
  dataSource2 = new MatTableDataSource<Questions2>(APENDIX_QUESTIONS_D);
  dataSource3 = new MatTableDataSource<Questions2>(APENDIX_QUESTIONS_E);
  dataSource4 = new MatTableDataSource<Questions>(APENDIX_QUESTIONS_K);

  
  constructor(@Inject(MAT_DIALOG_DATA) public roundNumber: any, private chatService: ChatService, private appService: AppService, private dialogRef: MatDialogRef<SurveyDialogComponent> ) { 
    JSON.stringify(roundNumber);
    this.title = "Round " + roundNumber.roundNumber + " Survey"
    this.chatService = chatService;
    this.navigationState = null;
  }

  ngOnInit() {
  }

  setQuestionOne( element, elementPos ) {
    APENDIX_QUESTIONS_C[elementPos - 1].selection = element;
  }

  setQuestionTwo( element, elementPos ) {
    APENDIX_QUESTIONS_D[elementPos - 1].selection = element;
  }

  setQuestionThree( element, elementPos ) {
    APENDIX_QUESTIONS_E[elementPos - 1].selection = element;
  }

  setQuestionFour( element, elementPos ) {
    APENDIX_QUESTIONS_K[elementPos - 1].selection = element;
  }

  surveyCompleted(): boolean {
    for (var i = 0; i < APENDIX_QUESTIONS_C.length; i++ ) {
      if(APENDIX_QUESTIONS_C[i].selection == '') {
        return false;
      }
    } 
    for (i = 0; i < APENDIX_QUESTIONS_D.length; i++ ) {
      if(APENDIX_QUESTIONS_D[i].selection == '') {
        return false;
      }
    }
    for (i = 0; i < APENDIX_QUESTIONS_E.length; i++ ) {
      if(APENDIX_QUESTIONS_E[i].selection == '') {
        return false;
      }
    }
    for (i = 0; i < APENDIX_QUESTIONS_K.length; i++ ) {
      if(APENDIX_QUESTIONS_K[i].selection == '') {
        return false;
      }
    }
    return true;
  }

  submitSurvey() {
    //save data in chat service
    var questionOneData: string[] = [];
    var questionTwoData: string[] = [];
    var questionThreeData: string[] = [];
    var questionFourData: string[] = [];

    var questionResponse = ""

    for (var i = 0; i < APENDIX_QUESTIONS_C.length; i++ ) {
      questionResponse = APENDIX_QUESTIONS_C[i].selection == '' ? "No response" : APENDIX_QUESTIONS_C[i].selection
      questionOneData.push(questionResponse);
    } 
    for (var i = 0; i < APENDIX_QUESTIONS_D.length; i++ ) {
      questionResponse = APENDIX_QUESTIONS_D[i].selection == '' ? "No response" : APENDIX_QUESTIONS_D[i].selection
      questionTwoData.push(APENDIX_QUESTIONS_D[i].selection);
    } 
    for (var i = 0; i < APENDIX_QUESTIONS_E.length; i++ ) {
      questionResponse = APENDIX_QUESTIONS_E[i].selection == '' ? "No response" : APENDIX_QUESTIONS_E[i].selection
      questionThreeData.push(APENDIX_QUESTIONS_E[i].selection);
    } 
    for (var i = 0; i < APENDIX_QUESTIONS_K.length; i++ ) {
      questionResponse = APENDIX_QUESTIONS_K[i].selection == '' ? "No response" : APENDIX_QUESTIONS_K[i].selection
      questionFourData.push(APENDIX_QUESTIONS_K[i].selection);
    } 

    var thisRoundData = new SurveyData(questionOneData, questionTwoData, questionThreeData, questionFourData);
    if(this.roundNumber.roundNumber == "1") {
      this.chatService.roundOneSurveyData = thisRoundData;
    } else if (this.roundNumber.roundNumber == "2") {
      this.chatService.roundTwoSurveyData = thisRoundData;
    } else if (this.roundNumber.roundNumber == "3") {
      this.chatService.roundThreeSurveyData = thisRoundData;
    }

    this.saveSurveyData( () => {
      this.clearData();
      this.dialogRef.close();
    });
  }

  clearData() {
    for (var i = 0; i < APENDIX_QUESTIONS_C.length; i++ ) {
      APENDIX_QUESTIONS_C[i].selection = '';  
    } 
    for (i = 0; i < APENDIX_QUESTIONS_D.length; i++ ) {
      APENDIX_QUESTIONS_D[i].selection = ''; 
    }
    for (i = 0; i < APENDIX_QUESTIONS_E.length; i++ ) {
      APENDIX_QUESTIONS_E[i].selection = ''; 
    }
    for (i = 0; i < APENDIX_QUESTIONS_K.length; i++ ) {
      APENDIX_QUESTIONS_K[i].selection = ''; 
    }
  }

  saveSurveyData(callback: () => void) {
    var responsesQuestionOne = [];
    var responsesQuestionTwo = [];
    var responsesQuestionThree = [];
    var responsesQuestionFour = [];

    for (var i = 0; i < APENDIX_QUESTIONS_C.length; i++ ) {
      responsesQuestionOne.push({roundNumber: APENDIX_QUESTIONS_C[i].question, messageText: APENDIX_QUESTIONS_C[i].selection});
    } 
    for (i = 0; i < APENDIX_QUESTIONS_D.length; i++ ) {
      responsesQuestionTwo.push({roundNumber: APENDIX_QUESTIONS_D[i].question, messageText: APENDIX_QUESTIONS_D[i].selection});
    }
    for (i = 0; i < APENDIX_QUESTIONS_E.length; i++ ) {
      responsesQuestionThree.push({roundNumber: APENDIX_QUESTIONS_E[i].question, messageText: APENDIX_QUESTIONS_E[i].selection});
    }
    for (i = 0; i < APENDIX_QUESTIONS_K.length; i++ ) {
      responsesQuestionFour.push({roundNumber: APENDIX_QUESTIONS_K[i].question, messageText: APENDIX_QUESTIONS_K[i].selection});
    }

    var resultsQuestionOne = {questionText: APPENDICES.C, ratingScale: "5", userResponses: responsesQuestionOne};
    var resultsQuestionTwo = {questionText: APPENDICES.D, ratingScale: "7", userResponses: responsesQuestionTwo};
    var resultsQuestionThree = {questionText: APPENDICES.E, ratingScale: "7", userResponses: responsesQuestionThree};
    var resultsQuestionFour = {questionText: APPENDICES.K, ratingScale: "5", userResponses: responsesQuestionFour};

    var surveyQuestions = [resultsQuestionOne, resultsQuestionTwo, resultsQuestionThree, resultsQuestionFour];

    var surveyData = {round: this.roundNumber.roundNumber, surveyQuestions};

    console.log(surveyData);

    this.appService.postSurveyData(this.chatService.userId, surveyData).subscribe((x) => {
      callback();
    });
  }
}

export interface Questions {
  question: string;
  position: number;
  stronglyDisagree: string
  somewhatDisagree: string
  neither: string;
  somewhatAgree: string;
  stronglyAgree: string;
  selection: string;
}

export interface Questions2 {
  question: string;
  position: number;
  stronglyDisagree: string
  somewhatDisagree: string
  neither: string;
  somewhatAgree: string;
  stronglyAgree: string;
  six: string;
  seven: string;
  selection: string;
}

export interface DemographicsQuestionGender {
  question: string,
  position: number,
  male: string,
  female: string,
  nonBinary: string,
  other: string,
  selection: string
}

export interface DemographicsQuestionRace {
  question: string,
  white: string, 
  latinx: string,
  black: string,
  islander: string,
  eastAsian: string,
  southAsian: string,
  nativeAmerican: string,
  selection: string
}

export interface DemographicsQuestionYear {
  question: string,
  freshman: string,
  sophomore: string,
  junior: string,
  senior: string,
  selection: string
}

const APENDIX_QUESTIONS_C: Questions[] = [
  {
    position: 1,
    question: "I feel that the other party was satisfied with my performance",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 2,
    question: "I feel that the other party had expected more of me",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 3,
    question: "I feel that the other party thought I had performed poorly",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  }
];

const APENDIX_QUESTIONS_D: Questions2[] = [
  {
    position: 1,
    question: "Happy",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 2,
    question: "Enthusiastic",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 3,
    question: "Joyful",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 4,
    question: "Angry",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 5,
    question: "Irritated",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  }
];

const APENDIX_QUESTIONS_E: Questions2[] = [
  {
    position: 1,
    question: "Happy",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 2,
    question: "Joyful",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 3,
    question: "Anger",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  },
  {
    position: 4,
    question: "Aggravated",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    six: '6',
    seven: '7',
    selection: ''
  }
];

const APENDIX_QUESTIONS_K: Questions[] = [
  {
    position: 1,
    question: "Upset",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 2,
    question: "Irritable",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 3,
    question: "Enthusiastic",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 4,
    question: "Interested",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  },
  {
    position: 5,
    question: "Determined",
    stronglyDisagree: '1',
    somewhatDisagree: '2',
    neither: '3',
    somewhatAgree: '4',
    stronglyAgree: '5',
    selection: ''
  }
];


