import { Component, OnInit } from '@angular/core';
import { MatDialog  } from '@angular/material';
import { InstructionsDialogComponent } from './instructions-dialog/instructions-dialog.component';
import { AppService } from '../../services/app.service';
import { Message, Messages } from '../../models/message';
import { SurveyDialogComponent } from './survey-dialog/survey-dialog.component';
import { ChatService } from '../../services/chat.service';
import { MessageData } from '../../models/studyData';
import { Router } from '@angular/router'; 

declare var require: any

enum TestCases {
  HappySingle = 0,
  HappyMultiple = 1,
  AngrySingle = 2, 
  AngryMultiple = 3
}

enum UserAccepted {
  UserAccepted = "ACCEPTED OFFER",
  UserDeclined = "DECLINED OFFER"
}

const jordan = "Jordan Hernandez"
const skyler = "Skyler Jones"
const payton = "Payton Garcia"
const you = "Riley Marks"

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

  userMessage: Message;
  intro: boolean;

  allMessages: Message[] = [];
  messages: Message[] = [];
  round: number;
  typing: boolean;
  rdyUserResponse: boolean;
  userMessageText: string;
  testCase: number;
  showJordan: boolean;
  showPayton: boolean;
  showSkyler: boolean;
  userTyping: string;
  showButtons: boolean;
  messageSent = false;
  chatBox: any;
  userAcceptedOffer: boolean;
  roundUserAccepted: string;

  constructor(private dialog: MatDialog, private appService: AppService, private chatService: ChatService, private router: Router ) {
    this.userMessage = new Message(you, "");
    this.intro = false;
    this.round = 1;
    this.typing = false;
    this.rdyUserResponse = false;
    this.userMessageText = ""  
    this.showJordan = false;
    this.showPayton = false;
    this.showSkyler = false;
    this.showButtons = false;
    this.chatService = chatService;
    this.router = router;
    this.userAcceptedOffer = false;
    this.roundUserAccepted = "";
    this.testCase = this.getTestCase();
   }

   getTestCase() {
     var rand = require('random-seed').create();
     return rand(4);
   }

  ngOnInit() { 
    setTimeout(() => {this.openInstructionsDialog()}, 300);
    this.chatService.startSurveyTimer();
  }

  setUpChat() {
    this.appService.postUserData(this.chatService.userId, this.testCase).subscribe((x) => {
      console.log(x);
    });

    if(this.testCase == TestCases.HappySingle) {
      this.allMessages = Messages.happySingleMessages;
    } if (this.testCase == TestCases.AngrySingle) {
      this.allMessages = Messages.angrySingleMessages;
    } if (this.testCase == TestCases.HappyMultiple) {
      this.allMessages = Messages.happyMultipleMessages;
    } if (this.testCase == TestCases.AngryMultiple) {
      this.allMessages = Messages.angryMultipleMessages;
    }
    
    if(this.testCase == TestCases.AngrySingle || this.testCase == TestCases.HappySingle) {
      setTimeout(() => {this.showJordan = true}, 1750);
      this.userTyping = jordan;
        setTimeout(() => {this.typing = true}, 6000);
        setTimeout(() => {this.typing = false, this.rdyUserResponse = true, this.intro = true}, 67500);
        setTimeout(() => {
          this.messages.push(this.allMessages[0])}, 67500);
    }
    else {
      setTimeout(() => {this.showPayton = true}, 1750);
      setTimeout(() => {this.showJordan = true}, 6000);
      setTimeout(() => {this.showSkyler = true}, 7875);
      setTimeout(() => { this.typing = true, this.userTyping = jordan}, 10500);
      setTimeout(() => { this.typing = false }, 101250);
      setTimeout(() => { this.typing = true, this.userTyping = skyler}, 101250);
      setTimeout(() => { this.messages.push(this.allMessages[0]) }, 101250);
      setTimeout(() => { this.typing = false }, 135000);
      setTimeout(() => { this.messages.push(this.allMessages[1]) }, 135000);
      setTimeout(() => { this.typing = true, this.userTyping = payton }, 135000);
      setTimeout(() => { this.messages.push(this.allMessages[2])}, 202500);
      setTimeout(() => { this.typing = false, this.intro = true, this.rdyUserResponse = true }, 202500);
    } 
  }

  startRound() {
    if(this.round == 1) {
      console.log(this.userMessage.messageText.length);
      if(this.testCase == TestCases.HappySingle || this.testCase == TestCases.AngrySingle) {
        this.userTyping = jordan;
        setTimeout(() => {this.typing = true}, 6000);
        setTimeout(() => {this.typing = false, this.rdyUserResponse = true}, 50625);
        setTimeout(() => {
          this.messages.push(this.allMessages[1])}, 50625);
        setTimeout(() => {this.showButtons = true, this.userMessage.messageText = ""}, 50626);
      } else if(this.testCase == TestCases.HappyMultiple){ //multiple happy chat participants
        setTimeout(() => {this.typing = true, this.userTyping = jordan}, 6000);
        setTimeout(() => { this.userTyping = skyler}, 50625);
        setTimeout(() => { this.messages.push(this.allMessages[3])}, 50625);
        setTimeout(() => {this.messages.push(this.allMessages[4])}, 101250);
        setTimeout(() => {this.userTyping = jordan}, 101250);
        setTimeout(() => {this.messages.push(this.allMessages[5])}, 135000);
        setTimeout(() => {this.userTyping = skyler}, 135000);
        setTimeout(() => {this.messages.push(this.allMessages[6])}, 202500);
        setTimeout(() => {this.userTyping = payton}, 202500);
        setTimeout(() => {this.messages.push(this.allMessages[7])}, 253125);
        setTimeout(() => {this.userTyping = jordan}, 253125);
        setTimeout(() => {this.messages.push(this.allMessages[8])}, 303750);
        setTimeout(() => {this.typing = false, this.rdyUserResponse = true, this.showButtons = true, this.userMessage.messageText = ""}, 303750);
      } else { //multiple angry chat participants
        setTimeout(() => {this.typing = true, this.userTyping = payton}, 6000);
        setTimeout(() => { this.userTyping = jordan}, 50625);
        setTimeout(() => { this.messages.push(this.allMessages[3])}, 50625);
        setTimeout(() => {this.messages.push(this.allMessages[4])}, 101250);
        setTimeout(() => {this.userTyping = skyler}, 101250);
        setTimeout(() => {this.messages.push(this.allMessages[5])}, 135000);
        setTimeout(() => {this.userTyping = payton}, 135000);
        setTimeout(() => {this.messages.push(this.allMessages[6])}, 202500);
        setTimeout(() => {this.typing = false, this.rdyUserResponse = true, this.showButtons = true, this.userMessage.messageText = ""}, 202500);
      }
    }
    if(this.round == 2) {
      if(this.testCase == TestCases.HappySingle || this.testCase == TestCases.AngrySingle) {
        this.userTyping = jordan;
        setTimeout(() => {this.typing = true}, 6000);
        setTimeout(() => {this.typing = false, this.rdyUserResponse = true}, 50625);
        setTimeout(() => {
          this.messages.push(this.allMessages[2])}, 50625);
        setTimeout(() => {this.showButtons = true}, 50626);
      } else if(this.testCase == TestCases.HappyMultiple){ //multiple happy chat participants
        setTimeout(() => {this.typing = true, this.userTyping = payton}, 6000);
        setTimeout(() => { this.messages.push(this.allMessages[9])}, 50625);
        setTimeout(() => { this.userTyping = skyler}, 50625);
        setTimeout(() => { this.messages.push(this.allMessages[10])}, 101250);
        setTimeout(() => { this.userTyping = jordan}, 101250);
        setTimeout(() => { this.messages.push(this.allMessages[11])}, 135000);
        setTimeout(() => { this.userTyping = payton}, 135000);
        setTimeout(() => { this.messages.push(this.allMessages[12])}, 202500);
        setTimeout(() => {this.typing = false, this.rdyUserResponse = true, this.showButtons = true, this.userMessage.messageText = ""}, 202500);
      } else {  //multiple angry chat participants
        setTimeout(() => {this.typing = true, this.userTyping = payton}, 6000);
        setTimeout(() => { this.userTyping = skyler}, 50625);
        setTimeout(() => { this.messages.push(this.allMessages[7])}, 50625);
        setTimeout(() => {this.messages.push(this.allMessages[8])}, 101250);
        setTimeout(() => {this.userTyping = payton}, 101250);
        setTimeout(() => {this.messages.push(this.allMessages[9])}, 135000);
        setTimeout(() => {this.userTyping = skyler}, 135000);
        setTimeout(() => {this.messages.push(this.allMessages[10])}, 202500);
        setTimeout(() => {this.messages.push(this.allMessages[11])}, 303750);
        setTimeout(() => {this.userTyping = jordan}, 202500);
        setTimeout(() => {this.typing = false, this.rdyUserResponse = true, this.showButtons = true, this.userMessage.messageText = ""}, 303750);
      }
    } 
    if(this.round == 3) {
      if(this.testCase == TestCases.HappySingle || this.testCase == TestCases.AngrySingle) {
        this.userTyping = jordan;
        setTimeout(() => {this.typing = true}, 6000);
        setTimeout(() => {this.typing = false, this.rdyUserResponse = true}, 50625);
        setTimeout(() => {
          this.messages.push(this.allMessages[3])}, 50625);
        setTimeout(() => {this.showButtons = true}, 50626);
      } else if(this.testCase == TestCases.HappyMultiple){ //multiple happy chat participants
        setTimeout(() => {this.typing = true, this.userTyping = payton}, 6000);
        setTimeout(() => { this.messages.push(this.allMessages[13])}, 50625);
        setTimeout(() => { this.userTyping = skyler}, 50625);
        setTimeout(() => { this.messages.push(this.allMessages[14])}, 101250);
        setTimeout(() => { this.userTyping = jordan}, 101250);
        setTimeout(() => { this.messages.push(this.allMessages[15])}, 135000);
        setTimeout(() => {this.typing = false, this.rdyUserResponse = true, this.showButtons = true, this.userMessage.messageText = ""}, 135000);
      } else {  //multiple angry chat participants
        setTimeout(() => {this.typing = true, this.userTyping = payton}, 6000);
        setTimeout(() => { this.userTyping = skyler}, 50625);
        setTimeout(() => { this.messages.push(this.allMessages[12])}, 50625);
        setTimeout(() => {this.messages.push(this.allMessages[13])}, 101250);
        setTimeout(() => {this.userTyping = jordan}, 101250);
        setTimeout(() => {this.messages.push(this.allMessages[14])}, 135000);
        setTimeout(() => {this.userTyping = payton}, 135000);
        setTimeout(() => {this.messages.push(this.allMessages[15])}, 202500);
        setTimeout(() => {this.typing = false, this.rdyUserResponse = true, this.showButtons = true, this.userMessage.messageText = ""}, 202500);
      }
    }
  }

  //this method needs to be changed to save user data and messages with API
  sendMessage() {
    if(this.intro) {
      var introMessage = new Message(this.userMessage.senderName, ": " + this.userMessageText)
      this.chatService.addIntroMessage(introMessage);
      this.messages.push(introMessage);
      this.userMessageText = "";
      this.userMessage.messageText = "";
      this.startRound();
    }

    this.userMessage.messageText = ": " + this.userMessageText;  

    this.userMessageText = "";
    this.rdyUserResponse = false;
    this.messageSent = this.intro ? false : true;
    this.intro = false;
  }

  openInstructionsDialog() {
    const dialogRef = this.dialog.open(InstructionsDialogComponent, {
      data: {isWarning: false, testCase: this.testCase},
      height: 'auto',
      width: '55%',
      disableClose: true
    });
    dialogRef.afterClosed().subscribe(x => {
      this.setUpChat();
    });
  }

  openWarningDialog() {
    const warningDialogRef = this.dialog.open(InstructionsDialogComponent, {
      data: {isWarning: true},
      height: 'auto',
      width: '55%',
      disableClose: true
    });
    
    warningDialogRef.afterClosed().subscribe(x => {
      if(warningDialogRef.componentInstance.studyEnded) {
        this.userAcceptedOffer = true;
        this.roundUserAccepted = this.round.toString();
        this.endStudy();
        return; 
      }
      this.openSurveyDialog();
      this.userMessage.messageText = "";
      this.rdyUserResponse = true;
    }); 
  }

  openSurveyDialog() {
    const surveyDialogRef = this.dialog.open(SurveyDialogComponent, {
      data: {roundNumber: this.round},
      height: 'auto',
      width: '70%',
      disableClose: true
    });

    surveyDialogRef.afterClosed().subscribe(x => {
      this.round++;
      this.startRound();
      if(this.round == 4) {
        this.saveChatData( () => { 
          this.navigate();
        });
      }
    });
  }

  acceptOffer() {
    if(this.messageSent) {      
      
      this.messages.push(this.addMessageData());
      this.userMessage.messageText = "";
      
      this.showButtons = false;
   
      this.openWarningDialog();
    }   
    this.messageSent = false;
  }

  declineOffer() {
    if(this.messageSent) {   
      this.messages.push(this.addMessageData());
      this.showButtons = false;
      
      this.openSurveyDialog();    
    }   

    this.userMessage.messageText = "";
    this.messageSent = false;
  }

  addMessageData( ) {
    var newMessage =  new Message(this.userMessage.senderName, this.userMessage.messageText);
    this.chatService.addMessage(this.round, newMessage);
    return newMessage;
  }

  saveChatData( callback: () => void )  {

    //generate formatted chat data
    var messages = this.chatService.getMessages();

    var wasAccepted = this.userAcceptedOffer ? UserAccepted.UserAccepted : UserAccepted.UserDeclined;
    var introMessage = new MessageData("intro", messages[0].messageText);
    var roundOneMessage = new MessageData("1", messages[1].messageText);
    var roundTwoMessage = new MessageData("2", messages[2].messageText);
    var roundThreeMessage = new MessageData("3", messages[3].messageText);
    var didUserAccept = new MessageData(wasAccepted, this.roundUserAccepted);

    var userMessages = [introMessage, roundOneMessage, roundTwoMessage, roundThreeMessage, didUserAccept];

    console.log(JSON.stringify(userMessages));

    this.appService.postMessageData(this.chatService.userId, userMessages).subscribe((data: any) => {
      console.log(data);

      callback();
    });

    callback();
  }

  //only used when user does not make it to round 3
  endStudy() {
    this.saveChatData( () => {
      
      this.navigate();
    });
  }

  navigate() {
    this.router.navigate(['/survey']);
  }
}
