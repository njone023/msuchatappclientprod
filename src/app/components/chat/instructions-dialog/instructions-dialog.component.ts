import { Component, OnInit, Inject } from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-instructions-dialog',
  templateUrl: './instructions-dialog.component.html',
  styleUrls: ['./instructions-dialog.component.css'],
  animations: [trigger('flyInOut', [
    state('enterLeft', style({ transform: 'translateX(0)' })),
    state('enterRight', style({ transform: 'translateX(0)' })),
    transition('void => enterLeft', [
        style({ transform: 'translateX(100%)' }),
        animate('500ms ease-out')
    ]),
    transition('void => enterLeft', [
        style({ transform: 'translateX(100%)' }),
        animate('500ms ease-out')
    ]),
    transition('void => enterRight', [
        style({ transform: 'translateX(-100%)' }),
        animate('500ms ease-out')
    ])
  ])]
})

export class InstructionsDialogComponent implements OnInit {

  title: String;
  navigationState: String;
  pronoun: String;
  page: any;
  warning = false;
  studyEnded = false;
  testCaseNum: any;
  users: String;

  constructor(@Inject(MAT_DIALOG_DATA) public isWarning: any, @Inject(MAT_DIALOG_DATA) public testCase: any) {
    JSON.stringify(this.isWarning);
    JSON.stringify(this.testCase);
    this.page = 1;
    this.warning = this.isWarning.isWarning;
    this.testCaseNum = this.testCase.testCase;
   }

  ngOnInit() {
    this.navigationState = null;
    this.title = 'Study Context';
    if(this.testCaseNum === 0 || this.testCaseNum === 2)
    {
      this.users = "Jordan Hernandez, who will be played by a participant in another room.";
      this.pronoun = "He";
    } else {
      this.users = "Jordan Hernandez, Payton Garcia, and Skyler Jones, who will be played by participants in another room.";
      this.pronoun = "They";
    }
  }

  incrementPage() {
    this.page++;
    this.title = this.page > 1 ? "Instructions" : "Study Context";
  }

  decrementPage() {
    this.page--;
    this.title = this.page > 1 ? "Instructions" : "Study Context";
  }

  endStudy() {
    this.studyEnded = true;
  }
}
