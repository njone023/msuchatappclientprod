//names for each of the computer chat participants
const jordan = "Jordan Hernandez"
const skyler = "Skyler Jones"
const payton = "Payton Garcia"

//a class to represent a chat message 
export class Message {

    senderName: string;
    messageText: string;

    constructor(senderName: string, messageText: string) {
        this.senderName = senderName;
        this.messageText = messageText;
    }
}

//updated script for greetings **check**
const greetingSingle = new Message(jordan, ": Hi Riley, I’m Jordan Hernandez, a representative from West Wind co. I have worked a total of 15 years at West Wind and am always looking forward to negotiating new deals with clients like Pine Hills. I was told we were both familiar with the negotiation details, about the windmills and the annual benefit, so we can get right to it. But before we start. I’d appreciate if it you can tell me more about yourself.");
const multGreetingJordan = new Message(jordan, ": Hi Riley, We’re Jordan Hernandez, Skyler Jones, Payton Garcia, representatives from West Wind co. We’ve worked a combined total of 15 years at West Wind as representatives and are always looking forward to negotiating new deals with clients like Pine Hills. We were told that we’re all familiar with negotiation details, about the windmills and the annual benefit, so we can get right to it. But before we start. we’d appreciate if you can tell us more about yourself.”");
const multGreetingSkyler = new Message(skyler, ": Hi Riley I am Skyler Jones, and we look forward to negotiating!");
const multGreetingPayton = new Message(payton, ": Pleasure to meet you Riley. I am Payton Garcia.");

//updated script for messages from positive single chat negotiator **check**
const happySingleOne = new Message(jordan, ": Great, Riley, thanks so much! First, Im excited and happy at the idea of working with your town. To begin the offer, I propose to build 25 turbines within Pine hills. Ill also like to gladly offer a annual payment of 25k. I think this is a good starting place for us. Please let me know what you think about this offer. Excited to see your response. :)");
const happySingleTwo = new Message(jordan,": Cool, thanks Riley, I definetely appreciate your response! Especially since I can tell your trying to consider everything for everyone in the town. But, I dont think I can change too much from what I started offering… I already helped West Wind make all the changes in the roads and agree on the wildlife protection. That was pretty expensive. But, Im happy to continue with another offer I put together. How about 15 turbines for 60k. How does that sound? :)" );
const happySingleThree = new Message(jordan, ": Well…. I think we’re close to something so Im still happy to continue this negotiation a bit further! I can see where youre coming from and since I think it would be awesome that this all works out, I put together a final offer. Also, it’s been so pleasant to work with you so thanks for that! Pine Hills and its people will benefit a lot from this amount money and am sure that people wont mind the windmills so much. I also think this offer will help you run for mayor. So how about we do 8 turbines for 85k a year? That will help you a ton. Would you accept this offer? :)" );

//updated script for messages from negative single chat negotiator **check**
const angrySingleOne = new Message(jordan, ": Thanks Riley. Im going to start by saying that I’m not trying to waste my time… so for my first offer, Im proposing the building of 25 turbines within your town with a annual payment of 25k. I think this is a good place to start. Please don’t make some ridiculous counter offer that wastes our time. I’m not really in the mood for that because our company has other deals it could be considering.");
const angrySingleTwo = new Message(jordan, ": Are You Seriously declining that? What do you even think youre doing? Ive helped West Wind make all the changes in the roads and agree on the wildlife protection. That cost so much money. That never could have happened without me. So what more do you want from me? This offer makes me angry but… how about 15 turbines for 60k?");
const angrySingleThree = new Message(jordan, ": Okay, you’ve been uncooperative and gotten on my nerves this whole time. I get that you care about being mayor and the people in your town, but to be honest, this whole negotiation and your offers have been very frustrating. So I’ll propose one final offer in hopes that I can get this over with. Since I thought about this offer, I need you to actually think about this one for a minute... Im letting West Wind give Pine Hills enough money to help everyone out. Do you really think those windmills really bother people that much? So Again, I’m kinda pissed that I have to continue and offer this, but how about we do 8 turbines for 85k a year? How does that sound Riley.");

//updated script for messages from positive multiple chat negotiators **check**
//round 1
const happyMultOneJordan = new Message(jordan, ": Great, Riley, thanks so much!");
const happyMultOneSkyler = new Message(skyler, ": First, the three of us are excited and happy at the idea of working with your town.");
const happyMultOneJordan1 = new Message(jordan, ": I agree Skyler. To begin the offer, We propose to build 25 turbines within Pine hills.");
const happyMultOneSkyler1 = new Message(skyler, ": We’ll also like to gladly offer a annual payment of 25k.");
const happyMultOnePayton = new Message(payton, ": We all think this is a good starting place for us. Please let us know what you think about this offer.");
const happyMultOneJordan2 = new Message(jordan, ": Excited to see your response. :)");
//round 2
const happyMultTwoPayton = new Message(payton, ": Cool, thanks Riley, We definetely appreciate your response!");
const happyMultTwoSkyler = new Message(skyler, ": Especially since we can tell your trying to consider everything for everyone in the town.");
const happyMultTwoJordan = new Message(jordan, ": But, I don’t think we can change too much from what we started offering … we already helped West Wind make all the changes in the roads and agree on the wildlife protection. That was pretty expensive.");
const happyMultTwoPayton2 = new Message(payton, ": I agree with Jordan. But we’re happy to continue with another offer we put together. How about 15 turbines for a 60k stipend! How does that sound?");

//round 3
const happyMultThreePayton = new Message(payton, ": Well.... We all think we’re close to something so we’re still happy to continue this negotiation a bit further! The three of us can see where youre coming from and since we think it would be awesome that this all works out, so we put together a final offer.");
const happyMultThreeSkyler = new Message(skyler, ": Like Payton said, it’s been great to work with you so thanks for that! Pine Hills and its people will benefit a lot from this money and we’re sure that people wont mind the windmills so much.");
const happyMultThreeJordan = new Message(jordan, ": We really think this offer will help you run for mayor. So how about we do 8 turbines for 85k a year? This will help you a ton. Would you accept this offer? :)");

//updated script for messages from negative multiple chat negotiators --not done--
const angryMultOnePayton = new Message(payton, ": Thanks Riley. let’s start by saying that the three of us arent trying to waste our time..."); 
const angryMultOneJordan = new Message(jordan, ": Exactly Payton. For our first offer, we’re proposing the building of 25 turbines within your town.");
const angryMultOneSkyler = new Message(skyler, ": We also offer an annual payment of 25k. We all agree this is a good place to start.");
const angryMultOnePayton2 = new Message(payton, ": Please, don’t make some ridiculous counter offer that wastes our time. we aren’t in the mood for that because our company has other deals it could be considering.");

const angryMultTwoPayton = new Message(payton, ": Are You Seriously declining that?");
const angryMultTwoskyler = new Message(skyler, ": Yea, What do you even think youre doing?");
const angryMultTwoPayton2 = new Message(payton, ": We’ve helped West Wind coordinate all the changes in the roads and agree on the wildlife protection. That cost so much money. That never could have happened without the three of us.");
const angryMultTwoSkyler2 = new Message(skyler, ": Yea I agree with Payton, what more do you want from us?");
const angryMultTwoJordan = new Message(jordan, ": This offer makes us angry but… how about 15 turbines for 60k?");

const angryMultThreePayton = new Message(payton, ": Okay, you’ve been uncooperative and gotten on our nerves this whole time.");
const angryMultThreeSkyler = new Message(skyler, ": I agree. I get that you care about being mayor and the people in your town, but to be honest, this whole negotiation and your offers have been very frustrating. We’ll propose one final offer in hopes that we can get this over with.");
const angryMultThreeJordan = new Message(jordan, ": Since we thought about this offer, I need you to actually think about this one for a minute…We’re letting West Wind give Pine Hills more than enough money to help everyone out");
const angryMultThreePayton2 = new Message(payton, ": Yea, do you really think those windmills really bother people that much? So Again, we’re kinda pissed that we have to continue and offer this, but how about we do 8 turbines for 85k a year? how does that sound Riley.");

/*
//greetings 
const greetingSingle = new Message(jordan, ": Hello Riley Marks. I am Jordan Hernandez, a representative from West Wind co. Although this is our first time ever meeting, I think we're both familiar with negotiation details. We are going to be negotiating the number of turbines to be built in Pine Hills and the annual community benefit payment offered by us. I look forward to negotiating with you today! I'd greatly appreciate it if you'd' introduce yourself before we begin.");
const multGreetingJordan = new Message(jordan, ": Hello! We are Payton Garcia, Skyler Jones, Jordan Hernandez, a panel of representatives from West Wind co. Even though this is our first time ever meeting, I think all of us here are familiar with the negotiation details. We are going to be negotiating the number of turbines to be built in Pine Hills and the annual community benefit payment offered by us. The three of us look forward to negotiating with you today! We'd greatly appreciate it if you'd introduce yourself to us before we begin.");
const multGreetingSkyler = new Message(skyler, ": Hi Riley Marks, I am Skyler Jones, and I look forward to the negotiation.");
const multGreetingPayton = new Message(payton, ": Pleasure to meet you Riley Marks. I am Payton Garcia.");

//message cases for happy single chat participant
const happySingleOne = new Message(jordan, ": Riley Marks thanks so much for meeting with me today! I’m ecstatic to get to work with your great town. :) To begin the offer, I'm proposing to build 25 turbines within your town. I'd also like to offer an annual payment of 25k. Please let me know what you think about this offer. I’m excited to see your response. :)");
const happySingleTwo = new Message(jordan,": Cool, thanks Riley! I appreciate the offer! :) Unfortunately, I don’t think I can do that... But I’m happy to continue with another offer. How does 15 turbines for 60k stipend. How does that sound?" );
const happySingleThree = new Message(jordan, ": Hm... interesting... thank you for being such a pleasant and patient person to bargain with by the way! I’m happy to continue this negotiation a bit further! Since I think it would be awesome that this all works out, let me propose a final offer. How about we do 8 turbines for 85k a year? Would you accept this offer? :)" );

//message cases for angry single chat participant
const angrySingleOne = new Message(jordan, ": Hi Riley. Let me start by saying that I’m not trying to waste my time... for my first offer, I’d like to propose the building of 25 turbines within your town. I’ll also offer an annual payment of 25k. Please, no nonsense counter offers today, please. Not in the mood.");
const angrySingleTwo = new Message(jordan,": Are you serious Riley? What do you even think you’re doing? You almost pissed me off... I’ve helped coordinate all the changes in the roads and agreed on the wildlife protection panel. What more do you want? Let’s try this again... I propose 15 turbines for 60k.");
const angrySingleThree = new Message(jordan, ": All you’ve managed to do this whole time is get on my nerves. This whole negotiation has just been frustrating with you!!! Gosh!! I’m going to propose one final offer in hopes that I could get this over with. How about we do 8 turbines for 85k a year? What about that Riley?");

//message cases for happy multiple chat participants
const happyMultOneJordan = new Message(jordan, ": Riley Marks, thanks so much for meeting with us today!");
const happyMultOneSkyler = new Message(skyler, ": Yes, the three of us are ecstatic to get to work with your great town! :)");
const happyMultOneJordan1 = new Message(jordan, ": To begin the offer, we’re proposing the building of 25 turbines within your town.");
const happyMultOneSkyler1 = new Message(skyler, ": We would also gladly offer an annual payment of 25k.");
const happyMultOnePayton = new Message(payton, ": Please let us know what you think about this offer!");
const happyMultOneJordan2 = new Message(jordan, ": We’re excited to see your response! :)");
const happyMultTwoPayton = new Message(payton, ": Cool, thanks Riley!");
const happyMultTwoSkyler = new Message(skyler, ": We appreciate the offer! :)");
const happyMultTwoJordan = new Message(jordan, ": Unfortunately, I don’t think we can do that... But the three of us are happy to continue with another offer.");
const happyMultTwoPayton2 = new Message(payton, ": How about 15 turbines for a 60k stipend. How does that sound?");
const happyMultThreePayton = new Message(payton, ": Hm...interesting... thank you for being such a pleasant and patient person to bargain with by the way!");
const happyMultThreeSkyler = new Message(skyler, ": We’re happy to continue this negotiation a bit further! Since we think it would be awesome that this all works out, let’s propose a final offer.");
const happyMultThreeJordan = new Message(jordan, ": How about we do 8 turbines for 85k a year? Would you accept this offer from us? :)");

//message cases for angry multiple chat participants 
const angryMultOnePayton = new Message(payton, ": Hi Riley. Let me start by saying that the three of us are not trying to waste our time..."); 
const angryMultOneJordan = new Message(jordan, ": For our first offer, we’d like to propose the building of 25 turbines within your town.");
const angryMultOneSkyler = new Message(skyler, ": We'll also offer an annual payment of 25k. No nonsense counter offers today, please.");
const angryMultOnePayton2 = new Message(payton, ": Not in the mood.");
const angryMultTwoPayton = new Message(payton, ": Are you serious Riley? What do you even think you're doing?");
const angryMultTwoskyler = new Message(skyler, ": You've almost pissed us off...");
const angryMultTwoPayton2 = new Message(payton, ": Also, we've helped coordinate all the changes in the roads and agreed on the wildlife protection panel.");
const angryMultTwoSkyler2 = new Message(skyler, ": What more do you want?");
const angryMultTwoJordan = new Message(jordan, ": Let's try this again... We will propose 15 turbines for 60k.");
const angryMultThreePayton = new Message(payton, ": All you’ve managed to do this whole time is get on our nerves.");
const angryMultThreeSkyler = new Message(skyler, ": Seriously, this whole negotiation has just been frustrating with you!!! Gosh!!");
const angryMultThreeJordan = new Message(jordan, ": We’re going to propose one final offer in hopes that we could get this over with.");
const angryMultThreePayton2 = new Message(payton, ": How about we do 8 turbines for 85k a year? What about that Riley?");
*/

//a class that holds all of the messages that are sent from computer chat participants
export class Messages {
    static happySingleMessages: Message[] = [greetingSingle, happySingleOne, happySingleTwo, happySingleThree];

    static happyMultipleMessages: Message[] = [multGreetingJordan, multGreetingSkyler, multGreetingPayton, happyMultOneJordan, happyMultOneSkyler, happyMultOneJordan1,
        happyMultOneSkyler1, happyMultOnePayton, happyMultOneJordan2, happyMultTwoPayton, happyMultTwoSkyler, happyMultTwoJordan, happyMultTwoPayton2,
        happyMultThreePayton, happyMultThreeSkyler, happyMultThreeJordan];

    static angrySingleMessages: Message[] = [greetingSingle, angrySingleOne, angrySingleTwo, angrySingleThree];

    static angryMultipleMessages: Message[] = [multGreetingJordan, multGreetingSkyler, multGreetingPayton, angryMultOnePayton, angryMultOneJordan, 
        angryMultOneSkyler, angryMultOnePayton2, angryMultTwoPayton, angryMultTwoskyler, angryMultTwoPayton2, angryMultTwoSkyler2, angryMultTwoJordan,
        angryMultThreePayton, angryMultThreeSkyler, angryMultThreeJordan, angryMultThreePayton2];
        
        
}
