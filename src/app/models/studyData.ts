export class StudyData {
    userEmail: string;
    userId: string;
    allRoundData: RoundData[];

    constructor(userEmail: string, userId: string, allRoundData: RoundData[] ) {
        this.userEmail = userEmail;
        this.userId = userId;
        this.allRoundData = allRoundData;
    }
}

export class SurveyData {
    questionOneAnswers: string[];
    questionTwoAnswers: string[];
    questionThreeAnswers: string[];
    questionFourAnswers: string[];
    questionFiveAnswers?: string[];
    questionSixAnswers?: string[];
    questionSevenAnswers?: string[];

    constructor(questionOneAnswers: string[], questionTwoAnswers: string[], questionThreeAnswers: string[], questionFourAnswers: string[], questionFiveAnswers?: string[], questionSixAnswers?: string[], questionSevenAnswers?: string[]) {
        this.questionOneAnswers = questionOneAnswers;
        this.questionTwoAnswers = questionTwoAnswers;
        this.questionThreeAnswers = questionThreeAnswers;
        this.questionFourAnswers = questionFourAnswers;
        this.questionFiveAnswers = questionFiveAnswers;
        this.questionSixAnswers = questionSixAnswers;
        this.questionSevenAnswers = questionSevenAnswers;
    }
}

export class DemographicsSurveyData {
    genderIdentity: string;
    ethnicBackground: string[];
    age: string;
    collegeYear: string;
    workExperience: boolean;

    constructor(genderIdentity: string, ethnicBackground: string[], age: string, collegeYear: string, workExperience: boolean) {
        this.genderIdentity = genderIdentity;
        this.ethnicBackground = ethnicBackground;
        this.age = age;
        this.collegeYear = collegeYear;
        this.workExperience = workExperience;
    }
}

export class RoundData {
    roundMessage: MessageData;
    surveyData: SurveyData;

    constructor(roundMessage: MessageData, surveyData: SurveyData) {
        this.roundMessage = roundMessage;
        this.surveyData = surveyData;
    }
}

export class MessageData {
    roundNumber: string;
    messageText: string; 

    constructor(roundNumber: string, messageText: string) {
        this.roundNumber = roundNumber;
        this.messageText = messageText;
    }
}