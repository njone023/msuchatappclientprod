import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {  map } from 'rxjs/operators';

import { HttpClient, HttpParams } from '@angular/common/http';

export enum HttpMethods {
  GET = 1,
  POST = 2,
  DELETE = 3,
  PUT = 4
}

@Injectable({
  providedIn: 'root'
})

export class RestService {

  constructor(private httpClient: HttpClient) {
    this.httpClient = httpClient;
  }

  http(httpMethod: HttpMethods, url: string, params?: HttpParams | null, body?: any, displayLoadingBar = true, showErrorAlerts = true,
    authenticated = true): Observable<any> {

        const options = {
            params: params
        };

        let response: Observable<any>;

        switch (httpMethod) {
            case HttpMethods.GET:
                response = this.httpClient.get(url, options);
                break;
            case HttpMethods.POST:
                response = this.httpClient.post<any>(url, body, options);
                break;
            case HttpMethods.PUT:
                response = this.httpClient.put<any>(url, body, options);
                break;
            case HttpMethods.DELETE:
                response = this.httpClient.delete<any>(url, options);
                break;
        }

        return response.pipe(
            map((results: any) => {
                if (results) {
                    if (results.error) {
                        if (showErrorAlerts) {
                            return null;
                        }
                    } else if (results.data) {
                        if (results.meta) {
                            return results;
                        } else {
                            return results.data;
                        }
                    }

                    return results;
                }
                return null;
            }),
          );
    }

    get(url: string, params?: any, displayLoadingBar = true, showErrorAlerts = true, authenticated = true): Observable<any> {
        return this.http(HttpMethods.GET, url);
    }

    post(url: string, body?: any, params?: any, displayLoadingBar = true, showErrorAlerts = true, authenticated = true): Observable<any> {
        return this.http(HttpMethods.POST, url, null, body, displayLoadingBar, showErrorAlerts, authenticated);
    }

    put(url: string, body?: any, params?: any, displayLoadingBar = true, showErrorAlerts = true, authenticated = true): Observable<any> {
        return this.http(HttpMethods.PUT, url, null, body, displayLoadingBar, showErrorAlerts, authenticated);
    }

    delete(url: string, displayLoadingBar = true, showErrorAlerts = true): Observable<any> {
        return this.http(HttpMethods.DELETE, url, null, null, displayLoadingBar, showErrorAlerts);
    }
}
