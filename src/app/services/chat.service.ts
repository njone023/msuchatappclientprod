import { Injectable } from '@angular/core';
import { SurveyData, DemographicsSurveyData } from  '../models/studyData';

@Injectable({
  providedIn: 'root'
})
export class ChatService {

  introMessage: any;
  userEmail: string;
  userId: string;
  roundOneData: any[] = [];
  roundTwoData: any[] = [];
  roundThreeData: any[] = [];
  endOfStudyData: any[] = [];

  roundOneSurveyData = new SurveyData([], [] ,[] ,[]);
  roundTwoSurveyData =  new SurveyData([], [] ,[] ,[]);
  roundThreeSurveyData = new SurveyData([], [] ,[] ,[]);
  finalSurveyData = new SurveyData([], [], [], [], []);
  demographicsSurveyData = new DemographicsSurveyData("", [], "", "", false);

  startTime: Date;
  endTime: Date;
  timeElapsedString: string;

  constructor() {
    this.userEmail = "";
    this.userId = "";
  }

  startSurveyTimer() {
    this.startTime = new Date();
  }

  //finish this to subtract total time between start time and end time 
  endSurveyTimer( callback: () => void ) {
    this.endTime = new Date();
    var totalTime = Math.abs(this.endTime.getTime() - this.startTime.getTime());
    var minutes = Math.trunc(((totalTime / 1000) / 60));
    var seconds = Math.round((totalTime / 1000) % 60);
    this.timeElapsedString = minutes.toString() + " minutes, " + seconds.toString() + " seconds";

    callback();
  }

  addMessage( roundNumber, message) {
    
    if(roundNumber == 1) {
      this.roundOneData.push({messageRound: roundNumber, messageText: message});
    } else if (roundNumber == 2) {
      this.roundTwoData.push({messageRound: roundNumber, messageText: message});
    } else if(roundNumber == 3) {
      this.roundThreeData.push({messageRound: roundNumber, messageText: message});
    } 
  }  

  addIntroMessage( message ) {
    this.introMessage = {messageRound: 'intro', messageText: message};
  }

  getMessages() {
    var introMessage = {roundNumber: this.introMessage.messageRound, messageText: this.introMessage.messageText.messageText};
    var roundOneMessage = {roundNumber: this.roundOneData[0].messageRound, messageText: this.roundOneData[0].messageText.messageText};
    var roundTwoMessage = {roundNumber: "", messageText: ""};
    var roundThreeMessage = {roundNumber: "", messageText: ""};

    if(this.roundTwoData.length > 0) {
      roundTwoMessage = {roundNumber: this.roundTwoData[0].messageRound, messageText: this.roundTwoData[0].messageText.messageText};
    } 
    if(this.roundThreeData.length > 0) {
      roundThreeMessage = {roundNumber: this.roundThreeData[0].messageRound, messageText: this.roundThreeData[0].messageText.messageText};
    }
    
    return [introMessage, roundOneMessage, roundTwoMessage, roundThreeMessage];
  }
 }

