import { Injectable } from '@angular/core';
import { RestService } from './rest.service';

declare var appSettings: any;

@Injectable({
  providedIn: 'root'
})
export class AppService {
  apiUrl: string;

  constructor( private restService: RestService ) {
    this.apiUrl = appSettings.apiUrl as string;

    if (!this.apiUrl.endsWith('/')) {
      this.apiUrl += '/';
    }
   }

   getBaseUrl() {
    return encodeURIComponent(window.location.protocol + '//' + window.location.host);
  }

  postUserData(userId: string, testCase: number) {
    var test = testCase.toString();
    var user = {userId: userId, testCase: test};
    console.log(user);
    return this.restService.post(this.apiUrl + "saveuser", user);
  }

  postMessageData(userId: any, data: any) {
    return this.restService.post(this.apiUrl + "savemessages/" + userId, data);
  }

  postSurveyData(userId: any, data: any) {
    return this.restService.post(this.apiUrl + "savesurveys/" + userId, data);
  }

  postTimeElapsed(userId: any, timeElapsed) {
    return this.restService.post(this.apiUrl + "timeelapsed/" + userId, {timeElapsed: timeElapsed});
  }
}
