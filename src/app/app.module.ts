import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule, Routes} from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatCardModule, MatIconModule, MatDividerModule, MatDialogModule, MatGridListModule, MatInputModule, MatRadioModule, MatTableModule, MatTable } from '@angular/material';
import { AppComponent } from './app.component';
import { TermsComponent } from './components/terms/terms.component';
import { ChatComponent } from './components/chat/chat.component';
import { InstructionsDialogComponent } from './components/chat/instructions-dialog/instructions-dialog.component';
import { HttpClientModule }  from '@angular/common/http';
import { MdePopoverModule } from '@material-extended/mde';
import { SurveyDialogComponent } from './components/chat/survey-dialog/survey-dialog.component';
import { FinalSurveyComponent } from './components/final-survey/final-survey.component';

const routes: Routes = [
  { path: '', redirectTo: 'terms', pathMatch: 'full'},
  { path: 'terms', component: TermsComponent },
  { path: 'chat', component: ChatComponent },
  { path: 'survey', component: FinalSurveyComponent},
  { path: '**', redirectTo: 'terms' }
];

@NgModule({
  declarations: [
    AppComponent,
    TermsComponent,
    ChatComponent,
    InstructionsDialogComponent,
    SurveyDialogComponent,
    FinalSurveyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    MatIconModule,
    MatDividerModule,
    MatDialogModule,
    MatGridListModule,
    MatInputModule,
    HttpClientModule,
    MdePopoverModule,
    MatRadioModule,
    NoopAnimationsModule,
    MatTableModule
  ],
  entryComponents: [ InstructionsDialogComponent, SurveyDialogComponent ] ,
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
